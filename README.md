# Home for Hubzilla

**Fr / En**

Initialement, les visiteurs ou membres non connectés de TheChangeBook voyaient la page de connection par défaut de Hubzilla / Initially, visitors or members not logged in to TheChangeBook saw the default Hubzilla login page :
<img title="TheChangebook" src="https://i.imgur.com/kinNEpO.png">

Pour mieux illustrer l'appartenance du réseau à la fediverse via un outil libre et décentralisé, une nouvelle page d'accueil a été imaginée / To better illustrate the belonging of the network to the fediverse via a free and decentralized tool, a new homepage has been imagined :
<img title="TheChangebook" src="https://i.imgur.com/lxknlpu.png">

Voir en action / See in action : [https://hub.thechangebook.org](https://hub.thechangebook.org)

## Langages utilisés / Used languages

Pour cette première version sont utilisés les langages suivants / For this first version the next languages are used : html, js et css.

## Utiliser ces fichiers sur son hub / Use these files on your hub
Avant d'utiliser les fichiers sur votre hub, apportez les modifications de votre choix / Before using the files on your hub, make any changes you require.

Le fichier home.html, le répertoire homme_connect et son contenu sont à placer à la racine de votre hub / The home.html file, the homme_connect directory and its contents should be placed at the root of your hub.

Ensuite, allez à admin/site/ de votre hub et saisissez "home.html" dans le champ "Page d'accueil du site à montrer aux visiteurs (par défaut : boîte de dialogue de connexion)" / Then go to admin/site/ of your hub and enter "home.html" in the field "Home page of the site to show to visitors (default: login dialog)".

Si vous placez les éléments ailleurs, indiquez l'emplacement correspond / If you place the elements elsewhere, specify the appropriate location.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/dandauge/home-for-hubzilla.git
git branch -M main
git push -uf origin main
```

## License
Ce projet est placé sous la licence MIT afin d'être en adéquation avec celle de Hubzilla / This project is licensed under the MIT license in order to be compatible with Hubzilla's license.
